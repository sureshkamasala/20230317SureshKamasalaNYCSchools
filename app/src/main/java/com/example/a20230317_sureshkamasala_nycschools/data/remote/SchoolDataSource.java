package com.example.a20230317_sureshkamasala_nycschools.data.remote;

import com.example.a20230317_sureshkamasala_nycschools.data.model.School;
import com.example.a20230317_sureshkamasala_nycschools.data.model.SchoolRating;

import java.util.List;

public interface SchoolDataSource {

    interface LoadSchoolsCallback {
        void onSchoolsListServiceSuccess(List<School> schoolList);
        void onDataNotAvailable();
        void onError();
    }

    interface LoadSchoolRateCallback {
        void onSchoolsRatingServiceSuccess(List<SchoolRating> schoolRatings);
        void onDataNotAvailable();
        void onError();
    }

    void getSchools(LoadSchoolsCallback callback);

    void getSchoolRatings(LoadSchoolRateCallback schoolRateCallback);
}
